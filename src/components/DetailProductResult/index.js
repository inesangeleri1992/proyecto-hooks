import React, { useEffect, useState } from 'react';
import { Grid, Typography, StylesProvider, CircularProgress, Card, Button } from '@material-ui/core';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { productDescriptionResult, productResult } from '../../redux/selectors';
import './DetailProductResult.css';
import { searchProductDescriptionById, searchProductById } from '../../redux/actions/search';

const DetailProductResults = () => {

    const dispatch = useDispatch();
    const productDetail = useSelector((state) => productResult(state));
    const [productId, setProductId] = useState('');
    const productsDescription = useSelector((state) => productDescriptionResult(state));
    const { id } = useParams();

    useEffect(() => {
        if (id && id !== productId) {
            setProductId(id);
            dispatch(searchProductById({ id }));
            dispatch(searchProductDescriptionById({ id }));
        }
    });

    const resultRender = () => {
        if (productDetail && productsDescription) {
            return (
                <StylesProvider injectFirst>
                    <Card >
                        <Grid container direction='row'>
                            <Grid item xs={8} >
                                <div className='columnDescriptionLeft'>
                                    {productDetail.pictures &&
                                        <img src={productDetail.pictures[0].url} className='img' />
                                    }
                                    <Typography className='descriptionTitleDetail'>Descripcion del producto</Typography>
                                    <Typography className='descriptionBody'>{productsDescription.plain_text}</Typography>
                                </div>
                            </Grid>
                            <Grid item xs={4} >
                                <div className='columnDescriptionRight'>
                                    <div className='marginBotton14'>
                                        <Typography className='soldDetail'>{productDetail.condition} - {productDetail.sold_quantity} vendidos</Typography>
                                    </div>
                                    <Typography className='productTitleDetail'>{productDetail.title}</Typography>
                                    <Typography className='price'>$ {new Intl.NumberFormat("de-De", { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(productDetail.price)}</Typography>
                                    <Button className='button'>Comprar</Button>
                                </div>
                            </Grid>
                        </Grid>
                    </Card>
                </StylesProvider>
            )
        } else {
            return <CircularProgress />
        }
    }

    return (
        <StylesProvider injectFirst>
            <div className='container'>
                {resultRender()}
            </div>
        </StylesProvider >
    )
}

export default DetailProductResults;