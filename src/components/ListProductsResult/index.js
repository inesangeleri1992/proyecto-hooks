import React, { useEffect, useState } from 'react';
import { Grid, Typography, Card, CircularProgress, StylesProvider } from '@material-ui/core';
import { useHistory, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import queryString from 'query-string';
import Icon from "@material-ui/core/Icon";

import { searchProduct } from '../../redux/actions/search';
import { productResultsFilter } from '../../redux/selectors';
import shipping from '../../../src/assets/images/ic_shipping.png';
import './ListProductsResult.css';

const ListProductsResult = () => {
    const history = useHistory();
    const location = useLocation();
    const dispatch = useDispatch();
    const [searchText, setSearchText] = useState('');
    const filterProduct = useSelector((state) => productResultsFilter(state));

    const iconShipping = (
        <Icon className='shipping'>
            <img alt="icon" src={shipping} />
        </Icon>
    );

    useEffect(() => {
        console.log('useEffect');
        const { search } = queryString.parse(location.search);
        if (search && searchText !== search) {
            setSearchText(search);
            dispatch(searchProduct({ search }));
        }
    });

    const handleItemClick = (index) => {
        const productId = filterProduct[index].id;
        history.push(`/items/${productId}`);
    };

    let resultRender;

    if (filterProduct) {
        resultRender = filterProduct.map((value, index) => {
            return (
                <Card onClick={() => handleItemClick(index)} key={index} className='card'>
                    <Grid container direction='row' justify='flex-start' alignItems='center'>
                        <Grid item xs>
                            <img src={value.thumbnail} className='image' />
                        </Grid>
                        <Grid item xs={8}>
                            <Grid container direction='column' justify='flex-start' alignItems='flex-start'>
                                <div className='marginBotton32'>
                                    <Grid item>
                                        <Grid container direction='row' justify='space-between' alignItems='center'>                                        
                                            <Typography className='priceList'>${new Intl.NumberFormat("de-De", { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(value.price)}</Typography>
                                            {value.shipping && value.shipping.free_shipping &&
                                                 iconShipping                                               
                                            }
                                        </Grid>
                                    </Grid>
                                </div>
                                <Grid item>                                   
                                    <Typography className='productTitle'>{value.title}</Typography>
                                </Grid>

                            </Grid>

                        </Grid>
                        <Grid item xs>
                            {value.address && value.address.state_name &&
                                <Typography className='cityTitle'>{value.address.state_name}</Typography>
                            }
                        </Grid>
                    </Grid>
                </Card>

            )
        })
    } else {
        resultRender = <CircularProgress />

    }

    return (
        <StylesProvider injectFirst>
            <div className='container'>
                {resultRender}
            </div>
        </StylesProvider>
    )

}

export default ListProductsResult;