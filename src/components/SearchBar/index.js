import React from 'react';
import { AppBar, Toolbar, Button, StylesProvider, InputBase } from "@material-ui/core";
import Icon from "@material-ui/core/Icon";

import search from '../../../src/assets/images/ic_Search.png';
import logo from '../../../src/assets/images/Logo_ML.png';
import './SearchBar.css'

const SearchBar = (props) => {

  const iconSearch = (
    <Icon className='iconSearch'>
      <img alt="search" src={search} />
    </Icon>
  );

  const iconLogo = (
    <Icon className='iconLogo'>
      <img alt="search" src={logo} />
    </Icon>
  );

  const handleSearchTextChange = event => {
    props.onChange(event.target.value);
  };

  const handleSearchTextClick = event => {
    props.onClick();
  };

  return (
    <StylesProvider injectFirst>
      <AppBar className='appbar' >
        <Toolbar className='toolbar'>
          <div>{iconLogo}</div>
          <InputBase
            className='inputRoot'
            placeholder="Nunca dejes de buscar"          
            fullWidth={true}
            value={props.value}
            onChange={handleSearchTextChange}
          />
          <div>
            <Button
              className='searchIconBotton'
              startIcon={iconSearch}
              onClick={handleSearchTextClick}
            />
          </div>
        </Toolbar>
      </AppBar>
    </StylesProvider>
  );
}
export default SearchBar;