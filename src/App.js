import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from './redux/store';

import ListProductsResult from './components/ListProductsResult';
import DetailProductResult from './components/DetailProductResult';
import Home from './pages/Home';
import  './App.css';

const store = configureStore();

const App = () => (
	
	<Provider store={store}>
		<Router>			
			<Home />
			<div >				
	 			<Route exact path="/items" component={ListProductsResult} /> 
				<Route exact path="/items/:id" component={DetailProductResult} />
			</div>
		</Router>
	</Provider>

);

export default App;



