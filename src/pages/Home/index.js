import React, { useState } from 'react';
import { Container, Breadcrumbs, Typography, StylesProvider } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';

import SearchBar from '../../components/SearchBar';
import { breadcrumbResults } from '../../redux/selectors';
import './Home.css';

export default () => {
    const breadcrumb = useSelector((state) => breadcrumbResults(state));
    const [searchText, setSearchText] = useState('');
    const history = useHistory();

    const handleSearchTextChange = newValue => {
        setSearchText(newValue);
    };

    const handleSearchTextClick = event => {
        history.push(`/items?search=${searchText}`);
    }

    let renderBreadcrumbs = null;
    if (breadcrumb && breadcrumb.length !== 0) {
        const valuesBreadcrumb = breadcrumb.filter(b => b.id === 'category')[0].values;
        renderBreadcrumbs = valuesBreadcrumb[0].path_from_root.map((value, index) => {
            return (
                <Typography className='textBreadcrumb' color="textPrimary" key={index} >{value.name}</Typography>
            )
        });
    }

    return (
        <StylesProvider injectFirst>
            <div style={{marginBottom:64}}>
                <SearchBar onClick={handleSearchTextClick} onChange={handleSearchTextChange} value={searchText} />
            </div>
            <div className='containerBreadcrumbs'>
                <Breadcrumbs
                    className='breadcrumbs'
                    separator={<NavigateNextIcon fontSize="small" />} aria-label="breadcrumb">
                    {renderBreadcrumbs}
                </Breadcrumbs>
            </div>
        </StylesProvider>
    )
}