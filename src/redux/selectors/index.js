import { get } from 'lodash';

export const isSearchLoading = state => get(state, 'search.isLoading');
export const productResults = state => get(state, 'search.productResults.data.results');
export const productResultsFilter = (state) => {
    if (!state.search.productResults) {
        return []
    }
    if (!state.search.productResults.data) {
        return []
    }
    if (!state.search.productResults.data.results) {
        return []
    }
    return state.search.productResults.data.results.slice(0, 4);
}
export const breadcrumbResults = state => get(state, 'search.productResults.data.filters');
export const productResult = state => get(state, 'search.productResult.data');
export const productDescriptionResult = state => get(state, 'search.productDescriptionResult.data');

