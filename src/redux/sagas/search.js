import { put, call, takeLatest } from 'redux-saga/effects';
import {
    SEARCH_PRODUCT_COMPLETE,
    SEARCH_PRODUCT_ERROR,
    SEARCH_PRODUCT_START,
    SEARCH_PRODUCTBYID_COMPLETE,
    SEARCH_PRODUCTBYID_START,
    SEARCH_PRODUCTBYID_ERROR,
    SEARCH_PRODUCTBYID_DESCRIPTION_COMPLETE,
    SEARCH_PRODUCTBYID_DESCRIPTION_ERROR,
    SEARCH_PRODUCTBYID_DESCRIPTION_START
} from '../../consts/actionTypes';

import { apiCall } from '../api'
export function* searchProduct({ payload }) {
    try {       
        const results = yield call(apiCall, `/sites/MLA/search?q=${payload.search}`, null, null, 'GET');      
        yield put({ type: SEARCH_PRODUCT_COMPLETE, results })
    }
    catch (error) {
        yield put({ type: SEARCH_PRODUCT_ERROR, error })
    }
}

export function* searchProductById({ payload }) {
    try {
       // console.log("Accion searchProductById---> ", payload);
        const results = yield call(apiCall, `/items/${payload.id}`, null, null, 'GET');
        yield put({ type: SEARCH_PRODUCTBYID_COMPLETE, results })
    }
    catch (error) {
        yield put({ type: SEARCH_PRODUCTBYID_ERROR, error })
    }
}

export function* searchProductDescriptionById({ payload }) {
    try {
       // console.log("Accion searchProductByIdDescription---> ", payload);
        const results = yield call(apiCall, `/items/${payload.id}/description`, null, null, 'GET');
        yield put({ type: SEARCH_PRODUCTBYID_DESCRIPTION_COMPLETE, results })
    }
    catch (error) {
        yield put({ type: SEARCH_PRODUCTBYID_DESCRIPTION_ERROR, error })
    }
}

export default function* search() {
    yield takeLatest(SEARCH_PRODUCT_START, searchProduct);
    yield takeLatest(SEARCH_PRODUCTBYID_START, searchProductById);
    yield takeLatest(SEARCH_PRODUCTBYID_DESCRIPTION_START, searchProductDescriptionById);
}