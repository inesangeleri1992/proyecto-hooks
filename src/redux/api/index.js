import axios from 'axios'; 


const baseURL= 'https://api.mercadolibre.com';


export const apiCall = (url, data,headers,method) => axios({
    method,
    url: baseURL + url, 
    data,
    headers
})