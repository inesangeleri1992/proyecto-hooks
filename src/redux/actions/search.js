import { SEARCH_PRODUCT_START, SEARCH_PRODUCTBYID_START, SEARCH_PRODUCTBYID_DESCRIPTION_START } from "../../consts/actionTypes";

export const searchProduct = payload => ({
    type: SEARCH_PRODUCT_START,
    payload
});

export const searchProductById = payload => ({
    type: SEARCH_PRODUCTBYID_START,
    payload
});

export const searchProductDescriptionById = payload => ({
    type: SEARCH_PRODUCTBYID_DESCRIPTION_START,
    payload
});