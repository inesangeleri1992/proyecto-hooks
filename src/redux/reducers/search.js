import {
    SEARCH_PRODUCT_COMPLETE,
    SEARCH_PRODUCT_ERROR,
    SEARCH_PRODUCT_START,
    SEARCH_PRODUCTBYID_COMPLETE,
    SEARCH_PRODUCTBYID_START,
    SEARCH_PRODUCTBYID_ERROR,
    SEARCH_PRODUCTBYID_DESCRIPTION_COMPLETE,
    SEARCH_PRODUCTBYID_DESCRIPTION_ERROR,
    SEARCH_PRODUCTBYID_DESCRIPTION_START
} from '../../consts/actionTypes';


const initialState = {};

export default function (state = initialState, action) {
    switch (action.type) {
        case SEARCH_PRODUCT_COMPLETE:
          console.log("reducer - SEARCH_PRODUCT_COMPLETE",action);
            return { ...state, isLoading: false, productResults: action.results, productResultsFilter: action.results }//productResultsFilter: action.results.data.results }
            break;
        case SEARCH_PRODUCT_START:
            return { ...state, isLoading: true }
            break;
        case SEARCH_PRODUCT_ERROR:          
            return { ...state, isLoading: false, productResults: null, productResultsFilter: null }
            break;
        case SEARCH_PRODUCTBYID_ERROR:
           // console.log("reducer - SEARCH_PRODUCTBYID_ERROR",action);
            return { ...state, isLoading: false, productResult: null }
            break;
        case SEARCH_PRODUCTBYID_START:
           // console.log("reducer SEARCH_PRODUCTBYID_START", action);
            return { ...state, isLoading: true }
            break;
        case SEARCH_PRODUCTBYID_COMPLETE:
           // console.log("reducer SEARCH_PRODUCTBYID_COMPLETE", action);
            return { ...state, isLoading: false, productResult: action.results }
            break;
        case SEARCH_PRODUCTBYID_DESCRIPTION_ERROR:
           // console.log("reducer SEARCH_PRODUCTBYID_DESCRIPTION_ERROR->ACTION", action);
            return { ...state, isLoading: false, productDescriptionResult: null }
            break;
        case SEARCH_PRODUCTBYID_DESCRIPTION_START:
          //  console.log("reducer SEARCH_PRODUCTBYID_DESCRIPTION_START->ACTION", action);
            return { ...state, isLoading: true }
            break;
        case SEARCH_PRODUCTBYID_DESCRIPTION_COMPLETE:
          //  console.log("reducer SEARCH_PRODUCTBYID_DESCRIPTION_START ->ACTION", action);
            return { ...state, isLoading: false, productDescriptionResult: action.results }
            break;
        default:
            return { ...state }
    }
}