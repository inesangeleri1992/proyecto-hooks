import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { StylesProvider } from "@material-ui/core/styles" // <-- import this component, and wrap your App.
import App from './App';


ReactDOM.render(<StylesProvider injectFirst>
    <App />
</StylesProvider>, document.getElementById('root'));
registerServiceWorker();


